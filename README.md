

### Set Env
```bash
export SERVICE_NAME=gitlab-runner
export CONTAINER_NAME=gitlab-runner
export THIS_IMAGE=gitlab/gitlab-runner
export TAG=latest
export DIR_DOCKER=docker
export THIS_VOL=gitlab-runner-vol
export DOCKER_PORT=xxx
export DOCKER_HOST=tcp://xxx:xxx
export DOCKER_SSH_PORT=22000
export DOCKER_HOST=ssh://xxx@xxx:xxx


# Docker Private Registry
export REGISTRY=index.docker.io
export REGISTRY_USER=user
export REGISTRY_PASS=xxx
export REGISTRY_GITLAB_TOKEN=xxx
export REGISTRY_GITLAB_HOST=https://gitlab.com/


envsubst < ${DIR_DOCKER}/docker-compose-template.yml > ${DIR_DOCKER}/docker-compose.yml


```

#### Start Docker gitlab-runner
```bash
docker-compose -f ${DIR_DOCKER}/docker-compose.yml down || true
docker pull ${THIS_IMAGE}:${TAG}
docker volume rm  ${THIS_VOL}
docker-compose -f ${DIR_DOCKER}/docker-compose.yml config
docker-compose -f ${DIR_DOCKER}/docker-compose.yml up -d

```

#### gitlab-runner init
```bash
docker cp ~/.ssh ${CONTAINER_NAME}:/home/gitlab-runner
docker exec -t ${CONTAINER_NAME} chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh

docker cp init.sh ${CONTAINER_NAME}:/home/gitlab-runner
docker exec -t ${CONTAINER_NAME} chmod +x /home/gitlab-runner/init.sh
docker exec -t ${CONTAINER_NAME} bash /home/gitlab-runner/init.sh
docker cp install-etc.sh ${CONTAINER_NAME}:/home/gitlab-runner
docker exec -t ${CONTAINER_NAME} bash /home/gitlab-runner/install-etc.sh


```


#### Setup Runner Register
```bash

# Run the register command:
docker run --rm -t -i -v gitlab-runner-vol:/etc/gitlab-runner gitlab/gitlab-runner:latest register


gitlab-runner register

# Enter gitlab-ci
https://gitlab.com/


# Enter Token
xxx

# Default Description [hostname]
Tok Presonal Runner

# Please enter the gitlab-ci tags for this runner (comma separated):
docker,python,gitlab-runner,tokdev

# Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker

# Please enter the Docker image (eg. ruby:2.1):
docker:20.10.18


```

#### Start Docker Runner
```bash
export REGISTRY=index.docker.io
export REGISTRY_USER=user
export REGISTRY_PASS=xxx

docker run -d --name gitlab-runner --restart unless-stopped \
    --env REGISTRY=${REGISTRY} \
    --env REGISTRY_USER=${REGISTRY_USER} \
    --env REGISTRY_PASS=${REGISTRY_PASS} \
    -v $(pwd)/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest

```

#### One-line registration command
```bash
##################################
# With Dcoker #
##################################

# At Docker
gitlab-runner stop

curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm64

chmod +x /usr/local/bin/gitlab-runner

## At Docker

curl -sSL https://get.docker.com/ | sh

useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

gitlab-runner start

usermod -aG docker gitlab-runner || true

docker login -u$REGISTRY_USER -p$REGISTRY_PASS $REGISTRY

gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "xxx" \
  --executor "docker" \
  --description "Tok Presonal Runner" \
  --docker-image "docker:20.10.18" \
  --tag-list "docker,python,gitlab-runner,tokdev" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

# # At VM
# sudo nano /etc/sudoers
# ...
# # User privilege specification
# root    ALL=(ALL:ALL) ALL
# Add new line
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
# ...


##################################
# With VM #
##################################

usermod -aG docker gitlab-runner

docker run -d --name gitlab-runner --restart unless-stopped \
    -v $(pwd)/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \ 
    gitlab/gitlab-runner:latest gitlab/gitlab-runner register \
    --non-interactive \
    --executor "docker" \
    --url "https://gitlab.com/" \
    --registration-token "xxx" \
    --description "Tok Presonal Runner" \
    --docker-image "docker:20.10.18" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --tag-list "docker,python,gitlab-runner,tokdev" \
    --run-untagged="true" \
    --locked="false" \
    --access-level="not_protected"
  
```

