# At Docker
gitlab-runner stop

curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm64

chmod +x /usr/local/bin/gitlab-runner

## At Docker

curl -sSL https://get.docker.com/ | sh

useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

gitlab-runner start

usermod -aG docker gitlab-runner || true

# # At VM
# sudo nano /etc/sudoers
# ...
# # User privilege specification
# root    ALL=(ALL:ALL) ALL
# Add new line
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
# ...

docker login -u$REGISTRY_USER -p$REGISTRY_PASS $REGISTRY

gitlab-runner register \
  --non-interactive \
  --url "${REGISTRY_GITLAB_HOST}" \
  --registration-token "${REGISTRY_GITLAB_TOKEN}" \
  --executor "docker" \
  --description "Tok Presonal Runner" \
  --docker-image "docker:19.03.12" \
  --tag-list "docker,python,gitlab-runner,tokdev" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

gitlab-runner restart 

